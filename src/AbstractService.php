<?php

namespace SpotifyApiClient;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

class AbstractService
{
    /** @var Configuration */
    protected $configuration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var RequestFactoryInterface */
    protected $requestFactory;

    /** @var StreamFactoryInterface */
    protected $streamFactory;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
        $this->httpClient = $configuration->getHttpClient();
        $this->requestFactory = $configuration->getRequestFactory();
        $this->streamFactory = $configuration->getStreamFactory();
    }
}
