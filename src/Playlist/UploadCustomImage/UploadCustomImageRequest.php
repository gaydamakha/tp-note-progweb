<?php

namespace SpotifyApiClient\Playlist\UploadCustomImage;

use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

class UploadCustomImageRequest
{
    /** @var string */
    private $playlistId;

    /** @var string */
    private $imageB64;

    public function __construct(string $playlistId, string $imageB64)
    {
        $this->playlistId = $playlistId;
        $this->imageB64 = $imageB64;
    }

    public function getRequestInterface(RequestFactoryInterface $requestFactory, StreamFactoryInterface $streamFactory): RequestInterface
    {
        return $requestFactory
            ->createRequest('PUT', 'https://api.spotify.com/v1/playlists/'.$this->playlistId.'/images')
            ->withHeader('Content-Type', 'image/jpeg')
            ->withBody($streamFactory->createStream($this->imageB64));
    }
}

