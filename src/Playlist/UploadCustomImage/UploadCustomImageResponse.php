<?php

namespace SpotifyApiClient\Playlist\UploadCustomImage;

class UploadCustomImageResponse
{
    /** @var bool */
    private $success;

    public function __construct(bool $success)
    {
        $this->success = $success;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }
}
