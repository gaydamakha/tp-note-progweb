<?php

namespace SpotifyApiClient\Playlist\UploadCustomImage;

use Psr\Http\Message\StreamFactoryInterface;
use SpotifyApiClient\AbstractService;

class UploadCustomImageService extends AbstractService
{
    /** @var StreamFactoryInterface */
    private $streamFactory;

    public function insert(UploadCustomImageRequest $uploadCustomImageRequest): UploadCustomImageResponse
    {
        $requestInterface = $uploadCustomImageRequest->getRequestInterface($this->requestFactory, $this->streamFactory);
        $response = $this->httpClient->sendRequest($requestInterface);
        return new UploadCustomImageResponse($response->getStatusCode() === 202);
    }
}
