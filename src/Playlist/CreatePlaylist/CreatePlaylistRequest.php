<?php

namespace SpotifyApiClient\Playlist\CreatePlaylist;

use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

class CreatePlaylistRequest
{
    /** @var string */
    private $userId;

    /** @var Playlist */
    private $newPlaylist;

    public function __construct(string $userId, Playlist $newPlaylist)
    {
        $this->userId = $userId;
        $this->newPlaylist = $newPlaylist;
    }

    public function getPayload(): array
    {
        $playlist = $this->newPlaylist;

        $payload = [
            'name' => $playlist->getName(),
            'public' => $playlist->isPublic(),
            'collaborative' => $playlist->isCollaborative(),
            'description' => $playlist->getDescription()
        ];

        return $payload;
    }

    public function getRequestInterface(RequestFactoryInterface $requestFactory, StreamFactoryInterface $streamFactory): RequestInterface
    {
        return $requestFactory
            ->createRequest('POST', "https://api.spotify.com/v1/users/".$this->userId."/playlists")
            ->withHeader('Content-Type', 'application/json')
            ->withBody($streamFactory->createStream(json_encode($this->getPayload())));
    }
}

