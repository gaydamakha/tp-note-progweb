<?php

namespace SpotifyApiClient\Playlist\CreatePlaylist;

class Playlist
{
    /** @var string */
    private $name;

    /** @var bool */
    private $public;

    /** @var bool */
    private $collaborative;

    /** @var string */
    private $description;

    public function __construct(string $name, ?bool $public, ?bool $collaborative, ?string $description)
    {
        $this->name = $name;
        $this->public = $public ?? true;
        $this->collaborative = $collaborative ?? false;
        $this->description = $description;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->public;
    }

    /**
     * @return bool
     */
    public function isCollaborative(): bool
    {
        return $this->collaborative;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
