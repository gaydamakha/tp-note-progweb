<?php

namespace SpotifyApiClient\Playlist\CreatePlaylist;

use Psr\Http\Message\StreamFactoryInterface;
use SpotifyApiClient\AbstractService;

class CreatePlaylistService extends AbstractService
{
    /** @var StreamFactoryInterface */
    private $streamFactory;

    public function insert(CreatePlaylistRequest $createPlaylistRequest): CreatePlaylistResponse
    {
        $requestInterface = $createPlaylistRequest->getRequestInterface($this->requestFactory, $this->streamFactory);
        $response = $this->httpClient->sendRequest($requestInterface);
        $jsonResponse = json_decode($response->getBody()->__toString());
        return new CreatePlaylistResponse(
            $jsonResponse->id,
            $jsonResponse->name,
            $jsonResponse->description,
            $jsonResponse->public,
            $jsonResponse->collaborative,
            $jsonResponse->href
        );
    }
}
