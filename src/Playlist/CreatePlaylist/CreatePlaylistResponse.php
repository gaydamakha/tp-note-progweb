<?php

namespace SpotifyApiClient\Playlist\CreatePlaylist;

class CreatePlaylistResponse
{
    /** @var string */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $desc;
    /** @var bool */
    private $public;
    /** @var bool */
    private $collaborative;
    /** @var string */
    private $href;

    public function __construct(string $id,
                                string $name,
                                string $desc,
                                bool $public,
                                bool $collaborative,
                                string $href)
    {
        $this->id = $id;
        $this->name = $name;
        $this->desc = $desc;
        $this->public = $public;
        $this->collaborative = $collaborative;
        $this->href = $href;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDesc(): string
    {
        return $this->desc;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->public;
    }

    /**
     * @return bool
     */
    public function isCollaborative(): bool
    {
        return $this->collaborative;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    public static function fromJsonString(string $json): self
    {
        $jsonObject = json_decode($json);
        $return = new self(
            $jsonObject->id,
            $jsonObject->name,
            $jsonObject->description,
            $jsonObject->public,
            $jsonObject->collaborative,
            $jsonObject->href);

        return $return;
    }
}
