<?php

namespace SpotifyApiClient\Playlist;

use SpotifyApiClient\AbstractService;
use SpotifyApiClient\Configuration;
use SpotifyApiClient\Playlist\CreatePlaylist\CreatePlaylistRequest;
use SpotifyApiClient\Playlist\CreatePlaylist\CreatePlaylistResponse;
use SpotifyApiClient\Playlist\CreatePlaylist\CreatePlaylistService;
use SpotifyApiClient\Playlist\CreatePlaylist\Playlist;
use SpotifyApiClient\Playlist\UploadCustomImage\UploadCustomImageRequest;
use SpotifyApiClient\Playlist\UploadCustomImage\UploadCustomImageResponse;
use SpotifyApiClient\Playlist\UploadCustomImage\UploadCustomImageService;

class PlaylistService extends AbstractService
{
    /** @var int */
    private $userId;

    public function __construct(Configuration $configuration, int $userId)
    {
        parent::__construct($configuration);
        $this->userId = $userId;
    }

    public function createPlaylist(Playlist $newPlaylist): CreatePlaylistResponse
    {
        $createPlaylistRequest = new CreatePlaylistRequest($this->userId, $newPlaylist);
        $createPlaylistService = new CreatePlaylistService($this->configuration);
        return $createPlaylistService->insert($createPlaylistRequest);
    }

    public function uploadCustomImage(string $playlistId, string $imageB64): UploadCustomImageResponse
    {
        $uploadCustomImageRequest = new UploadCustomImageRequest($playlistId, $imageB64);
        $uploadCustomImageService = new UploadCustomImageService($this->configuration);
        return $uploadCustomImageService->insert($uploadCustomImageRequest);
    }

//
//    public function deleteModel(int $modelId): DeleteModelResponse
//    {
//        $deleteModelRequest = new DeleteModelRequest($this->id, $modelId);
//        $deleteModelService = new DeleteModelService($this->configuration);
//        return $deleteModelService->handle($deleteModelRequest);
//    }
}
