<?php

namespace SpotifyApiClient\Playlist\RemoveTracks;

class TrackInPlaylist
{
    /** @var string */
    private $spotifyUri;

    /** @var array */
    private $positions;

    /**
     * TrackInPlaylist constructor.
     * @param string $spotifyUri
     * @param array $positions
     */
    public function __construct(string $spotifyUri, array $positions)
    {
        $this->spotifyUri = $spotifyUri;
        $this->positions = $positions;
    }

    /**
     * @return string
     */
    public function getSpotifyUri(): string
    {
        return $this->spotifyUri;
    }

    /**
     * @return array
     */
    public function getPositions(): array
    {
        return $this->positions;
    }
}