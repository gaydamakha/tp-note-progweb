<?php

namespace SpotifyApiClient\Playlist\RemoveTracks;

use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

class RemoveTracksRequest
{
    /** @var string */
    private $playlistId;

    /** @var array */
    private $tracksInPlaylist;

    public function __construct(string $playlistId, array $tracksInPlaylist)
    {
        $this->playlistId = $playlistId;
        $this->tracksInPlaylist = $tracksInPlaylist;
    }

    public function getPayload(): array
    {
        $tracks = $this->tracksInPlaylist;

        //TODO: serialize tracks in the payload
        $payload = [];

        return $payload;
    }

    public function getRequestInterface(RequestFactoryInterface $requestFactory, StreamFactoryInterface $streamFactory): RequestInterface
    {
        return $requestFactory
            ->createRequest('DELETE', 'https://api.spotify.com/v1/playlists/'.$this->playlistId.'/tracks')
            ->withHeader('Content-Type', 'application/json')
            ->withBody($streamFactory->createStream(json_encode($this->getPayload())));
    }
}

