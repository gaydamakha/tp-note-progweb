<?php

namespace SpotifyApiClient;

use SpotifyApiClient\Playlist\PlaylistService;

class Client
{
    /** @var Configuration */
    protected $configuration;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function playlist(string $userId): PlaylistService
    {
        return new PlaylistService($this->configuration, $userId);
    }
}
