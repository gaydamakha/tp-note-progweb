<?php

namespace Tests\Brand;

use SpotifyApiClient\Playlist\RemoveTracks\RemoveTracksRequest;
use SpotifyApiClient\Playlist\RemoveTracks\TrackInPlaylist;
use Tests\BaseTest;

class RemoveTracksTest extends BaseTest
{
    public function testConstructRequest(): void
    {
        $playlistId = "6rqhFgbbKwnb9MLmUQDhG6";
        $request = new RemoveTracksRequest($playlistId, [
            new TrackInPlaylist("4iV5W9uYEdYUVa79Axb7Rh", [0,3]),
            new TrackInPlaylist("1301WleyT98MSxVHPZCA6M", [7])
        ]);
        $requestInterface = $request->getRequestInterface($this->getRequestFactory(), $this->getStreamFactory());
        $this->assertEquals('DELETE', $requestInterface->getMethod());
        $this->assertEquals('application/json', $requestInterface->getHeader('Content-Type')[0]);
        $this->assertEquals('https://api.spotify.com/v1/playlists/'.$playlistId.'/tracks', $requestInterface->getUri()->__toString());
        //TODO: test body
    }
}
