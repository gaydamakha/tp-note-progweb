<?php

namespace Tests\Brand;

use SpotifyApiClient\Playlist\UploadCustomImage\UploadCustomImageRequest;
use Tests\BaseTest;

class UploadCustomImageTest extends BaseTest
{
    public function testConstructRequest(): void
    {
        $playlistId = "6rqhFgbbKwnb9MLmUQDhG6";
        $imageB64 = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlz
                    IHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2Yg
                    dGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGlu
                    dWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRo
                    ZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";

        $request = new UploadCustomImageRequest($playlistId, $imageB64);
        $requestInterface = $request->getRequestInterface($this->getRequestFactory(), $this->getStreamFactory());
        $this->assertEquals('PUT', $requestInterface->getMethod());
        $this->assertEquals('https://api.spotify.com/v1/playlists/'.$playlistId.'/images', $requestInterface->getUri()->__toString());
        $this->assertEquals('image/jpeg', $requestInterface->getHeader('Content-Type')[0]);
        $this->assertEquals($imageB64, $requestInterface->getBody()->__toString());
    }
}
