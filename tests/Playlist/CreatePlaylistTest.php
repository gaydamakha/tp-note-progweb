<?php

namespace Tests\Brand;

use Tests\BaseTest;
use SpotifyApiClient\Playlist\CreatePlaylist\CreatePlaylistRequest;
use SpotifyApiClient\Playlist\CreatePlaylist\CreatePlaylistResponse;
use SpotifyApiClient\Playlist\CreatePlaylist\Playlist;

class CreatePlaylistTest extends BaseTest
{
    public function testConstructRequest(): void
    {
        $request = new CreatePlaylistRequest("mikhail", new Playlist("My Awesome Rock Playlist", true, false, "Rock of 60's"));
        $requestInterface = $request->getRequestInterface($this->getRequestFactory(), $this->getStreamFactory());
        $this->assertEquals('POST', $requestInterface->getMethod());
        $this->assertEquals('https://api.spotify.com/v1/users/mikhail/playlists', $requestInterface->getUri()->__toString());
        $this->assertEquals('application/json', $requestInterface->getHeader('Content-Type')[0]);
        $this->assertEquals(json_encode([
            'name' => "My Awesome Rock Playlist",
            'public' => true,
            'collaborative' => false,
            'description' => "Rock of 60's"
        ]), $requestInterface->getBody()->__toString());
    }

    /** @dataProvider getPayloads */
    public function testDeserializeResponse(string $payloadPath,
                                            string $id,
                                            string $name,
                                            string $desc,
                                            bool $public,
                                            bool $collaborative,
                                            string $href): void
    {
        $response = file_get_contents($payloadPath);
        $response = CreatePlaylistResponse::fromJsonString($response);
        $this->assertEquals($id, $response->getId());
        $this->assertEquals($name, $response->getName());
        $this->assertEquals($desc, $response->getDesc());
        $this->assertEquals($public, $response->isPublic());
        $this->assertEquals($collaborative, $response->isCollaborative());
        $this->assertEquals($href, $response->getHref());
    }

    public function getPayloads(): array
    {
        return [
            'Playlist 1' => [
                __DIR__ . '/../../payloads/playlist_1.json',
                "6rqhFgbbKwnb9MLmUQDhG6",
                "Playlist 1",
                "Description 1",
                true,
                false,
                "https://api.spotify.com/v1/users/mikhail/playlists/6rqhFgbbKwnb9MLmUQDhG6"
            ],
            'Playlist 2' => [
                __DIR__ . '/../../payloads/playlist_2.json',
                "6rqhFgbbKwnb9MLmUQDhG6",
                "Playlist 2",
                "Description 2",
                false,
                true,
                "https://api.spotify.com/v1/users/mikhail/playlists/6rqhFgbbKwnb9MLmUQDhG6"
            ],
        ];
    }
}
